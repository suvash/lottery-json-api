# LotteryApp

## Requirements

### Elixir Runtime

Install an appropriate Elixir runtime. https://elixir-lang.org/install.html

### PostgreSQL

Run a PostgreSQL server locally. If using docker, use the following command to run an instance that can easily be cleaned up later.

```
docker run --name postgres-instance -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d postgres
```

To clean up the instance afterwards, run the following

```
docker rm -f postgres-instance
```

## Local development

### Installation

Install dependencies with `mix deps.get`

### Database setup

Create, Migrate and Seed database with `mix ecto.setup`

### Start the server

Run `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Testing

Run tests with `mix test`
