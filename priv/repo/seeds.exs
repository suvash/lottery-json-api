import LotteryApp.Account, only: [create_user: 1, total_users: 0]

case total_users() do
  0 -> for _ <- 1..100, do: create_user(%{points: 0})
  _ -> IO.puts("Users already exist.")
end
