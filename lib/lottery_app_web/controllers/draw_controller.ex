defmodule LotteryAppWeb.DrawController do
  use LotteryAppWeb, :controller

  alias LotteryApp.Reward

  action_fallback LotteryAppWeb.FallbackController

  def index(conn, _params) do
    drawn = Reward.draw()
    render(conn, "index.json", drawn)
  end
end
