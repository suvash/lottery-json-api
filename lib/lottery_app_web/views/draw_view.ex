defmodule LotteryAppWeb.DrawView do
  use LotteryAppWeb, :view
  alias LotteryAppWeb.UserView

  def render("index.json", %{users: users, timestamp: timestamp}) do
    %{
      users: render_many(users, UserView, "user.json"),
      timestamp: timestamp
    }
  end
end
