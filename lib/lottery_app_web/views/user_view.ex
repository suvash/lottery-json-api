defmodule LotteryAppWeb.UserView do
  use LotteryAppWeb, :view

  def render("user.json", %{user: user}) do
    %{id: user.id, points: user.points}
  end
end
