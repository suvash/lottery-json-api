defmodule LotteryApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      LotteryApp.Repo,
      # Start the Telemetry supervisor
      LotteryAppWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: LotteryApp.PubSub},
      # Start the Endpoint (http/https)
      LotteryAppWeb.Endpoint,
      # Start a worker by calling: LotteryApp.Worker.start_link(arg)
      # {LotteryApp.Worker, arg}
      LotteryApp.Reward
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LotteryApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    LotteryAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
