defmodule LotteryApp.Repo do
  use Ecto.Repo,
    otp_app: :lottery_app,
    adapter: Ecto.Adapters.Postgres
end
