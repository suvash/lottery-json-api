defmodule LotteryApp.Account do
  import Ecto.Query, warn: false
  alias LotteryApp.Repo

  alias LotteryApp.Account.User

  def list_users do
    Repo.all(User)
  end

  def total_users do
    Repo.aggregate(User, :count)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  def redistribute_users_points_between(min, max) do
    from(u in User,
      update: [set: [points: fragment("(floor(random() * (?+1-?)) + ?)::int", ^max, ^min, ^min)]]
    )
    |> Repo.update_all([])
  end

  def get_users_exceeding_points(points, number_of_users) do
    from(u in User, where: u.points > ^points, limit: ^number_of_users)
    |> Repo.all([])
  end
end
