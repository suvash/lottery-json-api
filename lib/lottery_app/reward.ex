defmodule LotteryApp.Reward do
  use GenServer

  alias LotteryApp.Account
  import DateTime, only: [utc_now: 0, truncate: 2]
  require Logger

  @server __MODULE__

  # API

  def start_link(_) do
    init_state = %{max_number: Enum.random(0..100), last_queried_at: nil}
    GenServer.start_link(@server, init_state, name: @server)
  end

  def draw do
    GenServer.call(@server, :draw)
  end

  # Callbacks

  def init(state) do
    Logger.debug("Starting #{@server} server")
    schedule_redistribution(0)
    {:ok, state}
  end

  def handle_call(:draw, _from, state) do
    Logger.debug("Handling :draw")
    handled_at = utc_now() |> truncate(:second)

    reply = %{
      users: Account.get_users_exceeding_points(state.max_number, 2),
      timestamp: state.last_queried_at
    }

    {:reply, reply, %{state | last_queried_at: handled_at}}
  end

  def handle_info(:redistribute, state) do
    Logger.debug("Handling :redistribute")
    schedule_redistribution()
    Account.redistribute_users_points_between(0, 100)
    new_max_number = Enum.random(0..100)
    Logger.debug("New max number is: #{new_max_number}")
    {:noreply, %{state | max_number: new_max_number}}
  end

  # Internal

  defp schedule_redistribution(secs \\ 60) do
    Process.send_after(self(), :redistribute, secs * 1000)
  end
end
