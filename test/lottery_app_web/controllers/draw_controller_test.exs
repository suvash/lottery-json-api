defmodule LotteryAppWeb.DrawControllerTest do
  use LotteryAppWeb.ConnCase

  alias LotteryApp.Account

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all users", %{conn: conn} do
      0..100
      |> Enum.map(&user_fixture(%{points: &1}))

      _ = get(conn, Routes.draw_path(conn, :index))
      conn = get(conn, Routes.draw_path(conn, :index))
      response = json_response(conn, 200)

      assert response["timestamp"]
      assert length(response["users"]) == 2
    end
  end

  defp user_fixture(attrs) do
    {:ok, user} =
      attrs
      |> Enum.into(%{points: 42})
      |> Account.create_user()

    user
  end
end
