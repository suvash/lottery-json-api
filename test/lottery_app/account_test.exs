defmodule LotteryApp.AccountTest do
  use LotteryApp.DataCase

  alias LotteryApp.Account

  describe "users" do
    alias LotteryApp.Account.User

    @valid_attrs %{points: 42}
    @update_attrs %{points: 43}
    @invalid_attrs %{points: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "total_users/0 returns the numbers of users" do
      assert 0 = Account.total_users()
      for _ <- 1..10, do: user_fixture()
      assert 10 = Account.total_users()
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.points == 42
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
      assert {:error, %Ecto.Changeset{}} = Account.create_user(%{points: -1})
      assert {:error, %Ecto.Changeset{}} = Account.create_user(%{points: 101})
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Account.update_user(user, @update_attrs)
      assert user.points == 43
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end

  test "redistribute_users_points_between/2 updates points for all users between min and max values" do
    user1 = user_fixture()
    user2 = user_fixture()
    user3 = user_fixture()

    assert user1.points == 42
    assert user2.points == 42
    assert user3.points == 42

    Account.redistribute_users_points_between(6, 9)

    assert Account.get_user!(user1.id).points in 6..9
    assert Account.get_user!(user2.id).points in 6..9
    assert Account.get_user!(user3.id).points in 6..9
  end

  test "get_users_exceeding_points/2 returns a number of users with above given points" do
    60..65
    |> Enum.map(&user_fixture(%{points: &1}))

    assert length(Account.get_users_exceeding_points(62, 2)) == 2
    assert length(Account.get_users_exceeding_points(62, 10)) == 3
    assert length(Account.get_users_exceeding_points(64, 10)) == 1
  end
end
