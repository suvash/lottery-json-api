defmodule LotteryApp.RewardTest do
  use LotteryApp.DataCase

  alias LotteryApp.Reward
  alias LotteryApp.Account

  @valid_attrs %{points: 42}

  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Account.create_user()

    user
  end

  test "draw/0 returns a result containing 2 users" do
    0..100
    |> Enum.map(&user_fixture(%{points: &1}))

    result = Reward.draw()
    assert length(result.users) == 2
  end

  test "draw/0 returns a result containing a timestamp" do
    0..100
    |> Enum.map(&user_fixture(%{points: &1}))

    _ = Reward.draw()
    result = Reward.draw()
    assert result.timestamp
  end
end
